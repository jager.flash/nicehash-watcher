namespace NHTicker {
    export class BackController {
        private showNotification = true;
        private walletAddress = "";

        constructor() {
            chrome.alarms.onAlarm.addListener(() => {
                var request = new XMLHttpRequest();

                chrome.storage.local.get(['address'], (item) => {
                    this.walletAddress = item.address || "";

                    request.onloadend = () => {
                      if(request.response) {
                        const response = JSON.parse(request.response);
                        const workers = response.result.workers;
                  
                        //notify about worker stopping
                        if(!workers.length) {
                          //show notify once, until worker turning on again
                          if(this.showNotification) {
                            this.showNotification = false;
                  
                            this.addNotification("Miner(s) Stopped", "", false);
                          }
                        } else {
                          if(!this.showNotification) {
                            const message = "Worker: '" + workers[0][0] + "' - speed: " + workers[0][1].a;
                            this.addNotification("Miner(s) In Work", message, true);
                  
                            this.showNotification = true;
                          }
                        }
                      }
                    }
              
                    if(this.walletAddress.length) {
                      request.open("GET", "https://api.nicehash.com/api?method=stats.provider.workers&addr=" + this.walletAddress, false);
                      request.send();
                    }
                });
              });
        }

        private addNotification(title: string, message: string, success: boolean) {
            chrome.notifications.create({
                type:     'basic',
                iconUrl:  success ? 'mining.png' : 'warning.png',
                title:    title,
                message:  message,
                buttons: [
                  {title: 'Keep it Flowing.'}
                ],
                priority: 0
              });
        }
    }
}

//Init background controller
const controller = new NHTicker.BackController();
