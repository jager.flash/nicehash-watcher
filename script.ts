namespace NHTicker {
  export interface IScope {
    address: string;
    editable: boolean;
    addr: string;
    workers: Array<[string, {a: number}, number, number, number, number, number]>;
    inWatching: boolean;
  }
  export class ViewController {
    constructor(private $scope) {
      let alarms = [];
      const algorithms = [ "Scrypt", "SHA256", "ScryptNf", "X11", "X13", "Keccak", "X15", "Nist5", "NeoScrypt", "Lyra2RE", "WhirlpoolX", "Qubit", "Quark", "Axiom", "Lyra2REv2", "ScryptJaneNf16", "Blake256r8", "Blake256r14", "Blake256r8vnl", "Hodl", "DaggerHashimoto", "Decred", "CryptoNight", "Lbry", "Equihash", "Pascal", "X11Gost", "Sia", "Blake2s", "Skunk" ];
        
      $scope.inWatching = false;
  
      $scope.cancelAlarm = () => {
        chrome.browserAction.setBadgeText({text: ''});
        chrome.alarms.clearAll();

        $scope.inWatching = false;
      }
  
      $scope.startAlarm = () => {
        if($scope.address.length) {
          if(alarms.length) {
            chrome.alarms.clearAll();
          }
          $scope.inWatching = true;
          chrome.alarms.create({periodInMinutes : 0.3});
          chrome.browserAction.setBadgeText({text: 'ON'});
        }
      }

      $scope.save = () => {
        $scope.editable = !$scope.editable;
        chrome.storage.local.set({address: $scope.address});

        this.getInfo();
      }
      
      $scope.walletClick = () => {
        if(!$scope.inWatching) {
          $scope.editable = true;
        }
      }

      this.init();
    }

    init() {
      chrome.storage.local.get(['address'], (item) => {
        this.$scope.$apply(()=>{
          this.$scope.address = item.address || "";
          this.$scope.editable = this.$scope.address.length === 0;
        })
        if(item.address.length) {
          this.getInfo();
        }
        chrome.alarms.getAll((alarms) => {
          this.$scope.$apply(() => {
            this.$scope.inWatching = alarms.length > 0;
          });
        });
      });
    }

    private getInfo() {
      var request = new XMLHttpRequest();
      
      request.onreadystatechange = () => {
        if(request.response) {
          var response = JSON.parse(request.response);
          var workers = response.result.workers;
    
          this.$scope.$apply(()=>{
            this.$scope.workers = workers;
          });
        }
      }
  
      request.open("GET", "https://api.nicehash.com/api?method=stats.provider.workers&addr=" + this.$scope.address, false);
      request.send();
    }

    private testAlarms() {
      chrome.alarms.getAll((alarms) => {
        if(alarms.length > 1) {
          chrome.alarms.clearAll();
        }
      })
    }
  }
}

NHTicker.ViewController.$inject = ["$scope"];
angular.module("app", [])
  .controller("ctrl", NHTicker.ViewController);